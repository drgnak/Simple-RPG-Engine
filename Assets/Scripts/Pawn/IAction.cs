// Copyright (c) 2015 Tony Nguyen
// See License.txt for license info

using UnityEngine;
using System;

// TODO: Document this
namespace SimpleRPGEngine
{
    public enum ActionTypes
    {
        Move,
        Attack,
        Menu,
    }
    public interface IAction
    {
        string Name { get; }
        bool IsEnabled { get; }
        void EnableAction();
        void DisableAction();
        ActionTypes ActionType { get; }
        Sprite GetIcon();
        bool DoesRequireOwnership();
    }
}
