// Copyright (c) 2015 Tony Nguyen
// See License.txt for license info

using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

namespace SimpleRPGEngine
{
    [RequireComponent(typeof(Pawn))]
    public class PawnMove : NetworkBehaviour, IAction
    {
        [SerializeField]
        private int m_MaxMoveDistance = 4;
        [SerializeField]
        private Sprite m_Icon;

        public string Name { get { return "Move"; } }
        public ActionTypes ActionType { get { return ActionTypes.Move; } }
        public bool IsMoving { get { return m_IsMoving; } }

        private bool m_CanSelect;

        protected GameMap m_Map;
        protected List<TilePathNode> m_ValidMoves;
        protected LinkedList<TilePathNode> m_Path;
        protected Pawn m_Pawn;
        private RPGInput m_Input;

        #region Movement Update Variables
        private Vector2 m_Destination;
        private Vector2 m_Start;
        private bool m_NextMove = true;
        protected bool m_IsMoving;
        private float m_MoveTimer;
        #endregion

        private void Start()
        {
            enabled = false;
            m_Pawn = GetComponent<Pawn>();
            if (m_Pawn == null) {
                Debug.LogError(gameObject.name + ": Missing pawn.");
            }
            m_Map = GameObject.FindWithTag("GameMap").GetComponent<GameMap>();
            if (m_Map == null) {
                Debug.LogError(gameObject.name + ": Missing GameMap");
            }
            m_Input = GameObject.FindWithTag("Input").GetComponent<RPGInput>();
            if (m_Input == null) {
                Debug.LogError(gameObject.name + ": Missing RPGInput");
            }
        }

        // Handle player input and pawn moving
        private void Update()
        {
            if (m_IsMoving) {
                if (m_Path.Count == 0) {
                    DisableAction();
                } else {
                    UpdateMovement();
                }
            } else if (m_CanSelect) {
                HandleInput();
            }
        }

        #region Action Interface Implementation
        public bool IsEnabled { get { return enabled; } }

        public void EnableAction()
        {
            enabled = true;
            m_CanSelect = true;
            m_ValidMoves = GetValidMoves();
            m_Map.SetNodesActive(m_ValidMoves.ToArray(), NodeActive.Move);
        }

        public void DisableAction()
        {
            m_IsMoving = false;
            m_Pawn.UpdateCurrentTile();
            enabled = false;
            ClearDraw();
            m_Path = null;
            m_ValidMoves = null;
            m_CanSelect = false;
        }

        public Sprite GetIcon()
        {
            return m_Icon;
        }

        public bool DoesRequireOwnership()
        {
            return true;
        }
        #endregion

        //public void Move(TilePathNode node)
        //{
        //    enabled = true;

        //    if (m_ValidMoves.Count > 0) {
        //        m_Map.SetNodesActive(m_ValidMoves.ToArray(), NodeActive.Move);
        //    }
        //}

        /// <summary>
        ///  Pawn moves to node, ignoring pathing
        /// </summary>
        /// <param name="node">node to move to</param>
        public virtual void Jump(TilePathNode node)
        {
            enabled = true;
            m_Path = new LinkedList<TilePathNode>();
            m_Path.AddFirst(node);
            StartMovement();
        }

        /// <summary>
        ///  Update path list based on mouse input
        /// </summary>
        private void HandleInput()
        {
            // Handle node highlight
            var node = m_Input.GetHighlightedNode();
            if (node != null) {
                UpdateDrawPath(node);
            }

            // Handle selection
            var selected = m_Input.GetSelectedNode();
            if (selected != null) {
                if (m_ValidMoves.Contains(selected)) {
                    m_CanSelect = false;
                    MovePawn(selected.X, selected.Y);
                } else {
                    DisableAction();
                }
            }
        }

        protected virtual void MovePawn(int x, int y)
        {
            if (m_Pawn.IsCurrentTurn) {
                m_Pawn.UseAp(this);
                StartMovement();
                Debug.Log(gameObject.name + " Client: " + "Recieved move (" +
                            x + ", " + y + ")");
            }
        }

        /// <summary>
        ///  Update the drawing of a path
        /// </summary>
        /// <param name="node">Current highlighted node</param>
        private void UpdateDrawPath(TilePathNode node)
        {
            if (m_ValidMoves != null && m_ValidMoves.Contains(node)) {
                // Clear old path
                DrawPath(false);

                // Select new path
                m_Path = GetPath(node);
                DrawPath(true);
            }
        }

        // TODO: Replace with GameMap.SetNodesHighlight()
        // Set highlight for nodes
        private void DrawPath(bool isPath)
        {
            if (m_Path != null) {
                foreach (var node in m_Path) {
                    node.IsHighlighted = isPath;
                }
            }
        }

        /// <summary>
        ///  Update pawn's real x/y to move towards the next node in the path
        ///  TODO: separate animation code from this class
        /// </summary>
        private void UpdateMovement()
        {
            if (m_NextMove) {
                // Setup next move
                var node = m_Path.First.Value;
                m_Start = m_Pawn.GridToWorld(m_Pawn.X, m_Pawn.Y);
                m_Destination = m_Pawn.GridToWorld(node.X, node.Y);
                m_Pawn.SetPosition(node.X, node.Y);
                m_NextMove = false;
            }

            // Move
            m_MoveTimer += Time.deltaTime * 2;
            transform.localPosition = Vector2.Lerp(m_Start, m_Destination, m_MoveTimer);

            // Update player values and remove item from queue
            if (m_MoveTimer >= 1) {
                m_NextMove = true;
                m_MoveTimer = 0;
                var node = m_Path.First.Value;
                m_Pawn.SetPosition(node.X, node.Y);
                m_Path.RemoveFirst();
            }
        }

        /// <summary>
        /// Wrapper for GameMap.GetValidMoves(TilePathNode, int)
        /// </summary>
        /// <returns>Array of valid move nodes</returns>
        protected List<TilePathNode> GetValidMoves()
        {
            return m_Map.GetValidMoves(m_Pawn.CurrentNode, m_MaxMoveDistance);
        }
        /// <summary>
        /// Wrapper for GameMap.GetPath(TilePathNode)
        /// </summary>
        /// <param name="node">Destination node</param>
        /// <returns>List of nodes in order from the origin node to the destination node</returns>
        protected LinkedList<TilePathNode> GetPath(TilePathNode node)
        {
            return m_Map.GetPath(node);
        }

        /// <summary>
        ///  Setup vars and enable movement along the path for UpdateMovement()
        /// </summary>
        protected void StartMovement()
        {
            if (m_Path != null) {
                if (m_Path.Count > 0) {
                    m_Path.Last.Value.AddOccupant(m_Pawn);
                    m_IsMoving = true;
                    m_NextMove = true;
                    ClearDraw();
                } else {
                    DisableAction();
                }
            }
        }

        /// <summary>
        ///  Clear all movement state variables from nodes
        /// </summary>
        private void ClearDraw()
        {
            if (m_ValidMoves != null) {
                m_Map.SetNodesActive(m_ValidMoves.ToArray(), NodeActive.None);
                m_Map.SetNodesHighlight(m_ValidMoves.ToArray(), false);
            }
        }
    }
}
