// Copyright (c) 2015 Tony Nguyen
// See License.txt for license info

using UnityEngine;
using UnityEngine.Networking;
using System.Linq;

namespace SimpleRPGEngine.UNet
{
    public class PawnAttackGenericUNet : PawnAttackGeneric, IAttackUNet
    {
        protected override bool Attack(TilePathNode node)
        {
            CmdAttack(node.X, node.Y);
            return false;
        }

        [Command]
        public void CmdAttack(int x, int y)
        {
            if (m_Pawn.IsCurrentTurn) {
                GetValidAttacks(m_Pawn.CurrentNode, m_Map, m_AllValidAttacks,
                                                m_MaxAttackDistance, m_MinAttackDistance);
                var node = m_Map.GetNode(x, y);
                bool isValid = AttackValidator.Validate(m_Pawn, node, m_Map, m_AllValidAttacks,
                    m_IsAoe);
                //if (m_IsAoe) {
                //    foreach (var validAttacks in m_AllValidAttacks) {
                //        if (validAttacks.Contains(node)) {
                //            // check for targets within aoe
                //            foreach (var tile in validAttacks) {
                //                if (tile.IsOccupied && tile.Occupant.OwnerId != m_Pawn.OwnerId) {
                //                    isValid = true;
                //                }
                //            }
                //        }
                //    }
                //} else {
                //    isValid = node.IsOccupied;
                //}
                RpcAttack(x, y, isValid);
                Debug.Log(gameObject.name + " Server: Sending attack to client (" + isValid + ")");
            }
        }

        [ClientRpc]
        public void RpcAttack(int x, int y, bool isValid)
        {
            // TODO: Implement
            if (isValid) {
                m_Pawn.UseAp(this);
                Debug.Log(gameObject.name + "Attack successful (" + isValid + ")");
            } else {
                DisableAction();
            }
        }
    }
}
