// Copyright (c) 2015 Tony Nguyen
// See License.txt for license info

using UnityEngine;
using UnityEngine.Networking;

namespace SimpleRPGEngine.UNet
{
    public class PawnUNet : Pawn
    {
        [Server]
        public void SetOwnerId(int id)
        {
            m_OwnerId = id;
            RpcSetOwnerId(id);
        }

        [ClientRpc]
        private void RpcSetOwnerId(int id)
        {
            m_OwnerId = id;
        }

        [Client]
        public override void EndTurn()
        {
            m_Ap = 0;
            CmdEndTurn();
            Debug.Log(gameObject.name + ": Ending turn.");
        }

        [Command]
        private void CmdEndTurn()
        {
            m_Ap = 0;
            m_IsCurrentTurn = false;
        }

        [Server]
        public override void Select()
        {
            RpcSelect();
        }

        [ClientRpc]
        private void RpcSelect()
        {
            if (isLocalPlayer) {
                var controller = GetComponent<IController>();
                if (controller != null) {
                    controller.Select();
                }
            } else {
                // Do things to show enemy's turn
            }
        }

        /// <summary>
        /// Consume one action point
        /// </summary>
        /// <param name="sender">IAction calling this method</param>
        [Server]
        public override void UseAp(IAction sender)
        {
            if (m_Ap <= 0) {
                Debug.LogWarning(gameObject.name + ": AP is at " + AP + " but an action is being used");
            }
            m_Ap -= 1;
            Debug.Log(gameObject.name + ": Current AP: " + AP);
        }
    }
}
