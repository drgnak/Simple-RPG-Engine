using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace SimpleRPGEngine.UNet
{
    interface IAttackUNet
    {
        void CmdAttack(int x, int y);
        void RpcAttack(int x, int y, bool isValid);
    }
}
