// Copyright (c) 2015 Tony Nguyen
// See License.txt for license info

using UnityEngine;
using UnityEngine.Networking;
using System.Linq;

namespace SimpleRPGEngine.UNet
{
    public class PawnAttackAdvUNet : PawnAttackAdv, IAttackUNet
    {
        protected override bool Attack(TilePathNode node)
        {
            Debug.Log(gameObject.name + ": Sending attack to server (" + node.X + ", " + node.Y + ")");

            CmdAttack(node.X, node.Y);
            return false;
        }

        [Command]
        public virtual void CmdAttack(int x, int y)
        {

            if (m_Pawn.IsCurrentTurn) {
                enabled = true;

                var node = m_Map.GetNode(x, y);

                GetValidAttacks(m_AttackType);

                bool isValid = AttackValidator.Validate(m_Pawn, node, m_Map, m_AllValidAttacks, m_IsAoe);
                //if (m_IsAoe) {
                //    foreach (var validAttacks in m_AllValidAttacks) {
                //        if (validAttacks.Contains(node)) {
                //            // check for targets within aoe
                //            foreach (var tile in validAttacks) {
                //                if (tile.IsOccupied && tile.Occupant.OwnerId != m_Pawn.OwnerId) {
                //                    isValid = true;
                //                }
                //                else {
                //                    Debug.Log(gameObject.name + ": Ally pawn detected " + 
                //                                    tile.IsOccupied +
                //                                    " " + tile.Occupant.OwnerId + " " + m_Pawn.OwnerId);
                //                }
                //            }
                //        }
                //    }
                //} else if (node.Occupant.OwnerId != m_Pawn.OwnerId) {
                //    isValid = node.IsOccupied;
                //}

                if (isValid) {
                    m_Pawn.UseAp(this);
                }
                RpcAttack(x, y, isValid);
                Debug.Log(gameObject.name + " Server: Sending attack to client (" + isValid + ")");
            }
        }

        [ClientRpc]
        public void RpcAttack(int x, int y, bool isValid)
        {
            // TODO: Implement
            if (isValid) {
                Debug.Log(gameObject.name + ": Attack successful (" + isValid + ")");
            } else {
                DisableAction();
            }
        }
    }
}
