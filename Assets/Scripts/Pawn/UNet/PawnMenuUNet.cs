// Copyright (c) 2015 Tony Nguyen
// See License.txt for license info

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

namespace SimpleRPGEngine.UNet
{
    public class PawnMenuUNet : PawnMenu
    {
        public override void BuildMenus()
        {
            RpcBuildMenus();
        }

        [ClientRpc]
        private void RpcBuildMenus()
        {
            if (isLocalPlayer) {
                AddActionButton("Attack", m_ActionButtons, ShowAttackMenu, HideActionMenu);
            }
            if (m_AllActions != null) {
                foreach (var action in m_AllActions) {
                    if (isLocalPlayer || !action.DoesRequireOwnership()) {
                        // Add a button in the appropriate category for each action
                        if (action.ActionType == ActionTypes.Move) {
                            AddActionButton(action.Name, m_ActionButtons,
                                            action.EnableAction, HideActionMenu);
                        } else if (action.ActionType == ActionTypes.Attack) {
                            AddActionButton(action.Name, m_AttackButtons,
                                            action.EnableAction, HideAttackMenu);
                        }
                    }
                }
            }
            AddActionButton("End Turn", m_ActionButtons, m_Pawn.EndTurn, HideAllMenus);
        }
    }
}
