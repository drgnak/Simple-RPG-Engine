// Copyright (c) 2015 Tony Nguyen
// See License.txt for license info

using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace SimpleRPGEngine.UNet
{
    public class PawnAttackLineUNet: PawnAttackLine, IAttackUNet 
    {
        protected override bool Attack(TilePathNode node)
        {
            CmdAttack(node.X, node.Y);
            return false;
        }

        [Command]
        public void CmdAttack(int x, int y)
        {
            if (m_Pawn.IsCurrentTurn) {
                enabled = true;
                GetValidLineAttacks(m_Pawn.CurrentNode, m_Map, m_AllValidAttacks,
                                                m_MaxAttackDistance, m_MinAttackDistance);
                var node = m_Map.GetNode(x, y);

                bool isValid = AttackValidator.Validate(m_Pawn, node, m_Map, m_AllValidAttacks,
                    m_IsAoe);

                RpcAttack(x, y, isValid);
                Debug.Log(gameObject.name + " Server: Sending attack to client (" + isValid + ")");
            }
        }

        [ClientRpc]
        public void RpcAttack(int x, int y, bool isValid)
        {
            // TODO: Implement
            if (isValid) {
                m_Pawn.UseAp(this);
                Debug.Log(gameObject.name + "Attack successful (" + isValid + ")");
            } else {
                DisableAction();
            }
        }
    }
}
