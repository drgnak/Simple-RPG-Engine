// Copyright (c) 2015 Tony Nguyen
// See License.txt for license info

using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;

namespace SimpleRPGEngine.UNet
{
    public class PawnMoveUNet : PawnMove
    {
        public override void Jump(TilePathNode node)
        {
            CmdJumpPawn(node.X, node.Y);
        }

        [Command]
        private void CmdJumpPawn(int x, int y)
        {
            if (m_Pawn.IsCurrentTurn) {

                var node = m_Map.GetNode(x, y);
                var isValid = false;

                if (!node.IsOccupied) {
                    enabled = true;
                    m_IsMoving = true;
                    m_Path = new LinkedList<TilePathNode>();
                    m_Path.AddFirst(node);
                    StartMovement();
                    isValid = true;
                }

                RpcJumpPawn(x, y, isValid);
            }
        }

        [ClientRpc]
        private void RpcJumpPawn(int x, int y, bool isValid)
        {
            if (isValid) {
                var node = m_Map.GetNode(x, y);
                enabled = true;
                m_IsMoving = true;
                m_Path = new LinkedList<TilePathNode>();
                m_Path.AddFirst(node);
                StartMovement();
            } else {
                DisableAction();
            }
        }

        protected override void MovePawn(int x, int y)
        {
            CmdMovePawn(x, y);
        }

        [Command]
        private void CmdMovePawn(int x, int y)
        {
            if (m_Pawn.IsCurrentTurn && m_Pawn.AP > 0) {
                enabled = true;
                // Check if player's selection is valid
                m_ValidMoves = GetValidMoves();
                var node = m_Map.GetNode(x, y);
                var isValid = m_ValidMoves.Contains(node);

                // For now, do what the clients do
                // TODO: Immediately move to ending position
                if (isValid) {
                    m_Path = GetPath(node);
                    StartMovement();
                    m_Pawn.UseAp(this);
                }
                RpcMovePawn(x, y, isValid);
                Debug.Log(gameObject.name + " Server: Sending move to client(" + isValid + ")");
            } else {
                Debug.LogWarning(gameObject.name + "Pawn is taking a move when it shouldn't");
            }
        }

        [ClientRpc]
        private void RpcMovePawn(int x, int y, bool isValid)
        {
            if (isValid == true) {
                if (!isLocalPlayer && !isServer) {
                    enabled = true;
                    m_ValidMoves = GetValidMoves();
                    m_Path = GetPath(m_Map.GetNode(x, y));
                }
                StartMovement();
                Debug.Log(gameObject.name + " Client: " + "Recieved move (" +
                            x + ", " + y + ")" + "(" + isValid + ")");
            } else {
                DisableAction();
            }
        }
    }
}
