// Copyright (c) 2015 Tony Nguyen
// See License.txt for license info

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace SimpleRPGEngine
{
    public class PawnAttackLine : PawnAttackBase
    {
        public override ActionTypes ActionType { get { return ActionTypes.Attack; } }

        #region Action Interface Implementation
        public override void EnableAction()
        {

            m_AllValidAttacks.Clear();
            enabled = true;
            // Get attack lines for all four directions
            GetValidLineAttacks(m_Pawn.CurrentNode, m_Map,
                                                m_AllValidAttacks, m_MaxAttackDistance, m_MinAttackDistance);

            foreach (var validAttacks in m_AllValidAttacks) {
                m_Map.SetNodesActive(validAttacks, NodeActive.Attack);
            }
        }

        public override void DisableAction()
        {
            enabled = false;
            ClearDraw(m_AllValidAttacks);
            m_AllValidAttacks.Clear();
            Debug.Log("BEEP BOOP");
        }
        #endregion
    }
}
