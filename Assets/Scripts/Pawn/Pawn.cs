// Copyright (c) 2015 Tony Nguyen
// See License.txt for license info

using UnityEngine;
using UnityEngine.Networking;

namespace SimpleRPGEngine
{
    public class PawnStatusEffects
    {
        public bool IsArmored { get; set; }
        public bool IsAtrophe { get; set; }
        public bool IsBearResolve { get; set; }
        public bool IsBearTactics { get; set; }
        public bool IsBlind { get; set; }
        public bool IsEncumbered { get; set; }
        public bool IsBurn { get; set; }
        public bool IsDehydrated { get; set; }
        public bool IsDespairCurse { get; set; }
        public bool IsEntombed { get; set; }
        public bool IsIsolationCurse { get; set; }
        public bool IsPain { get; set; }
        public bool IsParasite { get; set; }
        public bool IsStunned { get; set; }
        public bool IsTaunted { get; set; }
        public bool IsTigersFury { get; set; }
        public bool IsWolfsFury { get; set; }
        public bool IsWolfsResolve { get; set; }
    }

    public class Pawn : NetworkBehaviour
    {
        public string Name { get { return "Move"; } }

        [SerializeField][SyncVar] 
        protected int m_OwnerId;
        [SerializeField]
        private int m_Health = 20;
        [SerializeField][SyncVar]
        protected int m_Ap = 3;

        public int OwnerId { get { return m_OwnerId; } }
        public int Health { get { return m_Health; } }
        public int AP { get { return m_Ap; } }
        public bool IsCurrentTurn { get { return m_IsCurrentTurn; } }
        public bool IsStatusImmune { get; private set; }

        public int X { get; private set; }
        public int Y { get; private set; }
        public TilePathNode CurrentNode { get { return m_CurrentNode; } }
        public int TileSize { get; internal set; }

        [SyncVar]
        protected bool m_IsCurrentTurn;

        private GameMap m_Map;
        private TilePathNode m_CurrentNode;

        protected virtual void Start()
        {
            m_Map = GameObject.FindWithTag("GameMap").GetComponent<GameMap>();
            if (m_Map == null) {
                Debug.LogError(gameObject.name + ": Missing GameMap");
            }
            X = (int)(transform.localPosition.x / m_Map.tileSize);
            Y = (int)(transform.localPosition.y / m_Map.tileSize);
            transform.localPosition = new Vector2(X * m_Map.tileSize, Y * m_Map.tileSize);
            m_CurrentNode = m_Map.GetNode(X, Y);
            m_CurrentNode.AddOccupant(this);
            TileSize = m_Map.tileSize;
        }

        /// <summary>
        ///  Translate grid x/y to world coordinates
        /// </summary>
        /// <param name="x">Grid X coordinate</param>
        /// <param name="y">Grid Y coordinate</param>
        /// <returns>Position in world coordinates</returns>
        public Vector2 GridToWorld(int x, int y)
        {
            return new Vector2(x * TileSize, y * TileSize);
        }

        /// <summary>
        ///  Update the current tile for pawn position x y
        /// </summary>
        public void UpdateCurrentTile()
        {
            m_CurrentNode.ClearOccupant();
            m_CurrentNode = m_Map.GetNode(X, Y);
        }

        /// <summary>
        ///  Set X/Y of pawn
        /// </summary>
        /// <param name="x">Grid X coordinate of pawn</param>
        /// <param name="y">Grid Y coordinate of pawn</param>
        public void SetPosition(int x, int y) {
            X = x;
            Y = y;
        }

        /// <summary>
        ///  Take damage and checks if pawn is dead
        /// </summary>
        /// <param name="damage">Amount of damage to take</param>
        public void TakeDamage(int damage)
        {
            m_Health -= damage;
            if (Health <= 0) {
                Debug.Log(gameObject.name + "Dead");
                Destroy(gameObject);
            }
        }

        public virtual void StartTurn()
        {
            m_Ap = 3;
            m_IsCurrentTurn = true;
            Select();
        }

        public virtual void EndTurn()
        {
            m_Ap = 0;
            m_IsCurrentTurn = false;
        }

        public virtual void Select()
        {
            if (isLocalPlayer) {
                var controller = GetComponent<IController>();
                if (controller != null) {
                    controller.Select();
                }
            } else {
                // Do things to show enemy's turn
            }
        }

        /// <summary>
        /// Consume one action point
        /// </summary>
        /// <param name="sender">IAction calling this method</param>
        public virtual void UseAp(IAction sender)
        {
            if (m_Ap <= 0) {
                Debug.LogWarning(gameObject.name + ": AP is at " + AP + " but an action is being used");
            }
            m_Ap -= 1;
            Debug.Log(gameObject.name + ": Current AP: " + AP);
        }

        //public abstract PawnStatusEffects GetStatus();
    }
}
