// Copyright (c) 2015 Tony Nguyen
// See License.txt for license info

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

namespace SimpleRPGEngine
{
    /// <summary>
    /// Class responsible for exposing available action components to the player.
    /// This class searches for all components in the gameobject that implements IAction
    /// and creates UI widgets for each one, which runs EnableAction() when clicked
    /// </summary>
    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent(typeof(Pawn))]
    public class PawnMenu : NetworkBehaviour, IController
    {
        [SerializeField]
        private float m_Radius = 80f;
        [SerializeField]
        private float m_StartAngle = 25f;
        [SerializeField]
        private float m_ButtonAngle = 30f;

        protected IAction[] m_AllActions;
        protected List<GameObject> m_ActionButtons = new List<GameObject>();
        protected List<GameObject> m_AttackButtons = new List<GameObject>();
        protected List<GameObject> m_CurrentButtons;
        private Camera m_Camera;
        protected Pawn m_Pawn;


        // TODO: Different menus for different players
        // Use this for initialization
        private void Awake()
        {
            m_Camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
            if (m_Camera == null) {
                Debug.LogError(gameObject.name + ": Missing camera");
            }
            m_Pawn = GetComponent<Pawn>();
            if (m_Pawn == null) {
                Debug.LogError(gameObject.name + ": Missing pawn component");
            }
            enabled = false;

            m_AllActions = GetComponents<IAction>();
        }

        private void Update()
        {
            if (m_Pawn.AP > 0) {
                if (m_CurrentButtons != null) {
                    ShowMenu(m_CurrentButtons);
                } else if (!IsActionRunning()) {
                    ShowActionMenu();
                }
            } else {
                Deselect();
            }
        }

        public void Select()
        {
            ShowActionMenu();
            enabled = true;
        }

        public void Deselect()
        {
            HideAllMenus();
            m_Pawn.EndTurn();
            enabled = false;
        }

        public virtual void BuildMenus()
        {
            // Todo: implement
        }

        /// <summary>
        ///  Shows a menu containing all non-attack actions
        /// </summary>
        protected void ShowActionMenu()
        {
            // Don't select if an action is running
            if (!IsActionRunning()) {
                ShowMenu(m_ActionButtons);
            }
            Debug.Log(gameObject.name + ": Showing action menu");
        }

        /// <summary>
        ///  Shows a menu containing all attack actions
        /// </summary>
        protected void ShowAttackMenu()
        {
            // Don't select if an action is running
            if (!IsActionRunning()) {
                ShowMenu(m_AttackButtons);
            }
            Debug.Log(gameObject.name + ": Showing attack menu");
        }

        /// <summary>
        /// Position buttons within buttons in a circle around the pawn and SetActive
        /// </summary>
        /// <param name="buttons">List of buttons to activate</param>
        protected void ShowMenu(List<GameObject> buttons)
        {
            enabled = true;
            var collider = GetComponent<BoxCollider2D>();
            var worldPosition = new Vector3(transform.position.x + collider.size.x / 2,
                                            transform.position.y + collider.size.y / 2,
                                            transform.position.z);
            var menuPosition = m_Camera.WorldToScreenPoint(worldPosition);
            var radius = m_Radius / m_Camera.GetComponent<MouseCameraControl>().ZoomRatio;

            for (var i = 0; i < buttons.Count; i++) {
                buttons[i].transform.position = GetCirclePosition(menuPosition, radius,
                                                                i * m_ButtonAngle + m_StartAngle);
                buttons[i].SetActive(true);
            }
            m_CurrentButtons = buttons;
        }

        /// <summary>
        ///  Hide all menus
        /// </summary>
        protected void HideAllMenus()
        {
            HideActionMenu();
            HideAttackMenu();
            Debug.Log(gameObject.name + ": Hiding menu");
        }

        /// <summary>
        ///  Hide menu for non-attack actions
        /// </summary>
        protected void HideActionMenu()
        {
            foreach (var button in m_ActionButtons) {
                button.SetActive(false);
            }

            if (m_CurrentButtons == m_ActionButtons) {
                m_CurrentButtons = null;
            }
        }

        /// <summary>
        ///  Hide menu for attack actions
        /// </summary>
        protected void HideAttackMenu()
        {
            foreach (var button in m_AttackButtons) {
                button.SetActive(false);
            }

            if (m_CurrentButtons == m_AttackButtons) {
                m_CurrentButtons = null;
            }
        }

        /// <summary>
        /// Checks every action to see if at least one is currently enabled
        /// </summary>
        /// <returns>Returns true if there is an enabled action</returns>
        private bool IsActionRunning()
        {
            bool isRunning = false;
            foreach (var action in m_AllActions) {
                if (action.IsEnabled) {
                    isRunning = true;
                }
            }
            return isRunning;
        }

        /// <summary>
        /// Get a random position along a circle's circumference
        /// </summary>
        /// <param name="center">World position of the center of the circle</param>
        /// <param name="radius">Radius of the circle</param>
        /// <returns>Vector3 position in world units</returns>
        private Vector3 GetCirclePosition(Vector3 center, float radius, float degree)
        {
            Vector3 pos;
            pos.x = center.x + radius * Mathf.Sin(degree * Mathf.Deg2Rad);
            pos.y = center.y + radius * Mathf.Cos(degree * Mathf.Deg2Rad);
            pos.z = center.z;
            return pos;
        }
        
        /// <summary>
        /// Add a new button to buttonList and attach an action to it
        /// </summary>
        /// <param name="name">The name of the action to be displayed</param>
        /// <param name="buttonList">List for the new button to be added to</param>
        /// <param name="call">Method called when button is clicked</param>
        /// <param name="hideMenu">Method called to hide menu when clicked</param>
        protected void AddActionButton(string name, List<GameObject> buttonList, 
                                        UnityEngine.Events.UnityAction call, 
                                        UnityEngine.Events.UnityAction hideMenu)
        {
            GameObject o = (GameObject)Instantiate(Resources.Load("GUI/Button"));

            if (o != null) {
                var test = o.GetComponentInChildren<Text>();
                test.text = name;

                o.SetActive(false);
                o.transform.SetParent(GameObject.Find("Canvas").transform);
                var b = o.GetComponent<Button>();
                b.onClick.AddListener(call);
                b.onClick.AddListener(hideMenu);
                buttonList.Add(o);
            } else {
                Debug.LogError(gameObject.name + ": Button is null");
            }
        }
    }
}
