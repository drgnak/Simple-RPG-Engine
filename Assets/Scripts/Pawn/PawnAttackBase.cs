// Copyright (c) 2015 Tony Nguyen
// See License.txt for license info

using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Linq;
using System.Collections.Generic;

namespace SimpleRPGEngine
{
    public abstract class PawnAttackBase: NetworkBehaviour, IAction
    {

        [SerializeField]
        private string m_ActionName = "Attack";
        [SerializeField]
        private Sprite m_Icon;
        [SerializeField]
        private int m_Damage = 1;
        [SerializeField]
        protected int m_MaxAttackDistance = 6;
        [SerializeField]
        protected int m_MinAttackDistance = 1;
        [SerializeField]
        protected bool m_IsAoe;
        [SerializeField]
        protected bool m_TargetAllies;

        public string Name { get { return m_ActionName; } }
        public abstract ActionTypes ActionType { get; }

        protected Pawn m_Pawn;
        protected List<TilePathNode[]> m_AllValidAttacks = new List<TilePathNode[]>();
        protected GameMap m_Map;
        protected RPGInput m_Input;

        protected virtual void Start()
        {
            enabled = false;
            m_Pawn = GetComponent<Pawn>();
            if (m_Pawn == null) {
                throw new NullReferenceException(gameObject.name + ": Missing Pawn");
            }
            m_Map = GameObject.FindWithTag("GameMap").GetComponent<GameMap>();
            if (m_Map == null) {
                throw new NullReferenceException(gameObject.name + ": Missing GameMap");
            }
            m_Input = GameObject.FindWithTag("Input").GetComponent<RPGInput>();
            if (m_Input == null) {
                throw new NullReferenceException(gameObject.name + ": Missing RPGInput");
            }
        }

        /// <summary>
        ///  Handle player input and node selection within Update()
        /// </summary>
        protected virtual void Update()
        {
            var node = m_Input.GetHighlightedNode();
            foreach (var validAttacks in m_AllValidAttacks) {
                UpdateHighlight(node, validAttacks);
            }

            var selected = m_Input.GetSelectedNode();
            if (selected != null) {
                    SelectNode(selected);
            }
        }

        #region Action Interface Implementation
        public bool IsEnabled { get { return enabled; } }

        public abstract void EnableAction();

        public abstract void DisableAction();

        public Sprite GetIcon()
        {
            return m_Icon;
        }

        public bool DoesRequireOwnership()
        {
            return true;
        }
        #endregion

        // removes node states and highlight
        protected void ClearDraw(List<TilePathNode[]> allValidAttacks)
        {
            foreach (var validAttacks in allValidAttacks) {
                ClearDraw(validAttacks);
            }
        }

        protected void ClearDraw(TilePathNode[] validAttacks)
        {
            m_Map.SetNodesActive(validAttacks, NodeActive.None);
            m_Map.SetNodesHighlight(validAttacks, false);
        }

        // Update highlight when mouse hovers over node
        protected void UpdateHighlight(TilePathNode node, TilePathNode[] validAttacks)
        {
            if (validAttacks.Contains<TilePathNode>(node) && m_IsAoe) {
                m_Map.SetNodesHighlight(validAttacks, true);
            } else {
                m_Map.SetNodesHighlight(validAttacks, false);
            }
        }

        // Handle what happens after a node is selected
        protected virtual void SelectNode(TilePathNode node)
        {
            bool isValid = UNet.AttackValidator.Validate(m_Pawn, node, m_Map, m_AllValidAttacks, m_IsAoe);
            if (isValid) {
                if (m_IsAoe) {
                    foreach (var validAttack in m_AllValidAttacks) {
                        for (var i = 0; i < validAttack.Count(); i++) {
                            if (validAttack[i].IsOccupied && validAttack[i].Occupant.OwnerId != m_Pawn.OwnerId) {
                                Attack(node);
                            }
                        }
                    }
                } else {
                    Attack(node);
                }
            }
            DisableAction();
        }

        protected virtual bool Attack(TilePathNode node)
        {
            // TODO: Implement
            if (node.Occupant.OwnerId != m_Pawn.OwnerId) {
                node.Occupant.TakeDamage(m_Damage);
                Debug.Log(gameObject.name + "Attack successful");
                return true;
            } else {
                Debug.Log(gameObject.name + "Attack unsuccessful");
                return false;
            }
        }

        protected static void GetValidAttacks(TilePathNode startNode, GameMap map, List<TilePathNode[]> allAttacks,
                                                int maxAttackDistance, int minAttackDistance = 0)
        {
            if (startNode == null) {
                throw new NullReferenceException("sourceNode is null");
            }
            if (allAttacks == null) {
                throw new NullReferenceException("allAttacks is null");
            }
            if (map == null) {
                throw new NullReferenceException("map is null");
            }

            allAttacks.Clear();
            allAttacks.Add(map.GetValidAttack(startNode, maxAttackDistance, minAttackDistance));
        }

        protected static void GetValidLineAttacks(TilePathNode startNode, GameMap map, List<TilePathNode[]> allAttacks,
                                                    int maxAttackDistance, int minAttackDistance = 0)
        {
            if (startNode == null) {
                throw new NullReferenceException("sourceNode is null");
            }
            if (allAttacks == null) {
                throw new NullReferenceException("allAttacks is null");
            }
            if (map == null) {
                throw new NullReferenceException("map is null");
            }

            allAttacks.Clear();

            allAttacks.Add(map.GetValidAttackLine(startNode,
                            SettlersEngine.Direction.East, maxAttackDistance, minAttackDistance));
            allAttacks.Add(map.GetValidAttackLine(startNode,
                            SettlersEngine.Direction.North, maxAttackDistance, minAttackDistance));
            allAttacks.Add(map.GetValidAttackLine(startNode,
                            SettlersEngine.Direction.South, maxAttackDistance, minAttackDistance));
            allAttacks.Add(map.GetValidAttackLine(startNode,
                            SettlersEngine.Direction.West, maxAttackDistance, minAttackDistance));
        }
    }
}
