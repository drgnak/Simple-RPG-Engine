// Copyright (c) 2015 Tony Nguyen
// See License.txt for license info

using System.Collections.Generic;
using System;
using UnityEngine;

namespace SimpleRPGEngine
{

    public class PawnAttackGeneric : PawnAttackBase
    {
        public override ActionTypes ActionType { get { return ActionTypes.Attack; } }

        #region Action Interface Implementation
        public override void EnableAction()
        {
            enabled = true;
            GetValidAttacks(m_Pawn.CurrentNode, m_Map, m_AllValidAttacks,
                                            m_MaxAttackDistance, m_MinAttackDistance);

            for (var i = 0; i < m_AllValidAttacks.Count; i++) {
                m_Map.SetNodesActive(m_AllValidAttacks[i], NodeActive.Attack);
            }
        }

        public override void DisableAction()
        {
            enabled = false;
            ClearDraw(m_AllValidAttacks);
            m_AllValidAttacks.Clear();
            Debug.Log("BEEP BOOP");
        }
        #endregion
    }
}
