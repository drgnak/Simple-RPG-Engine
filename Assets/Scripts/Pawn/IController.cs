// Copyright (c) 2015 Tony Nguyen
// See License.txt for license info

namespace SimpleRPGEngine
{
    public interface IController
    {
        void Select();
        void Deselect();
    }
}
