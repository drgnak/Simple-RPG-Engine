// Copyright (c) 2015 Tony Nguyen
// See License.txt for license info

using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleRPGEngine
{
    public enum ActionItemType { Jump, Push, InflictStatus, Buff };
    public enum AttackType { Normal, Line }
    [RequireComponent(typeof(PawnMove))]
    public class PawnAttackAdv : PawnAttackBase
    {
        [Serializable]
        public struct Action
        {
            [SerializeField]
            public ActionItemType ActionType;
            [SerializeField]
            public int Value;
        }

        [SerializeField]
        protected AttackType m_AttackType;
        [SerializeField]
        protected Action[] m_ActionItems;

        public override ActionTypes ActionType { get { return ActionTypes.Attack; } } 

        private TilePathNode[] m_SelectedNodes;
        
        private PawnMove m_PawnMove;
        protected bool m_IsSelectingAttack = true;
        protected bool m_IsSelectingAction;
        // Store AoE setting here
        protected bool m_IsAoeSetting;
        private int m_TotalActions;
        private int m_ActionIndex;
        private TilePathNode[] m_ValidSelections;
        private List<TilePathNode> m_AttackSelections = new List<TilePathNode>();

        #region Action Interface Implementation
        public override void EnableAction()
        {
            enabled = true;
            // Search for attacks

            GetValidAttacks(m_AttackType);

            foreach (var validAttacks in m_AllValidAttacks) {
                m_Map.SetNodesActive(validAttacks, NodeActive.Attack);
            }

            m_IsAoe = m_IsAoeSetting;
            m_IsSelectingAttack = true;
            m_IsSelectingAction = false;
        }

        public override void DisableAction()
        {
            enabled = false;
            ClearDraw(m_AllValidAttacks);
            m_IsSelectingAttack = false;
            m_IsSelectingAction = false;
        }
        #endregion

        protected override void Start()
        {
            base.Start();
            m_IsAoeSetting = m_IsAoe;
            m_TotalActions = m_ActionItems.Length;
            m_SelectedNodes = new TilePathNode[m_TotalActions];
            m_PawnMove = GetComponent<PawnMove>();
            if (m_PawnMove == null) {
                Debug.LogError(gameObject.name + ": Missing PawnMove");
            }
        }

        protected override void Update()
        {
            if (m_IsSelectingAttack) {
                // Attack selection
                base.Update();
            } else if (m_ActionIndex < m_TotalActions) {
                // Player is currently selecting a node for an action
                if (m_IsSelectingAction) {
                    HandleInput();
                }

                // Player is done selecting a node
                if (!m_IsSelectingAction) {
                    SetupAction(m_ActionIndex + 1);
                }
            } else {
                // Finished getting player selection for all actions
                foreach (var selection in m_AttackSelections) {
                    Attack(selection);
                }
                for (var i = 0; i < m_ActionItems.Length; i++) {
                    if (m_ActionItems[i].ActionType == ActionItemType.Jump) {
                        m_PawnMove.Jump(m_SelectedNodes[i]);
                        Debug.Log("Performing jump.");
                    }
                }
                Debug.Log(gameObject.name + ": Done.");
                DisableAction();
            }
        }

        /// <summary>
        /// Handle player highlight and selection
        /// </summary>
        private void HandleInput()
        {
            var node = m_Input.GetHighlightedNode();
            if (node != null) {
                UpdateHighlight(node, m_ValidSelections);
            }

            var selected = m_Input.GetSelectedNode();
            if (selected != null) {
                if (m_ValidSelections.Contains(selected)) {
                    m_SelectedNodes[m_ActionIndex] = selected;
                    ClearDraw(m_ValidSelections);
                    m_IsSelectingAction = false;
                } else {
                    DisableAction();
                }
            }
        }

        protected void SetupAction(int index) {
            m_ActionIndex = index;
            if (index < m_TotalActions) {
                switch (m_ActionItems[index].ActionType) {
                    case ActionItemType.Jump:
                        m_ValidSelections = m_Map.GetValidMoves(m_Pawn.CurrentNode, m_ActionItems[m_ActionIndex].Value,
                                                                        m_ActionItems[m_ActionIndex].Value).ToArray();
                        m_Map.SetNodesActive(m_ValidSelections, NodeActive.Move);
                        m_IsAoe = false;
                        m_IsSelectingAction = true;
                        Debug.Log(gameObject.name + ": Move action.");
                        break;
                    case ActionItemType.Buff:
                        Debug.Log(gameObject.name + ": Buff action.");
                        break;
                    case ActionItemType.InflictStatus:
                        Debug.Log(gameObject.name + ": InflictStatus action.");
                        break;
                    case ActionItemType.Push:
                        Debug.Log(gameObject.name + ": Push action.");
                        break;
                    //case ActionItemType.Move:
                    //    m_ValidSelections = m_Pawn.Map.GetValidMoves(m_Pawn.CurrentTile.Node, m_ActionItems[m_ActionIndex].Max).ToArray();
                    //    GameMap.map.SetNodesActive(m_ValidSelections, NodeActive.Move);
                    //    m_IsAoe = false;
                    //    break;
                    default:
                        m_IsSelectingAction = false;
                        break;
                        //case ActionItemType.AttackGeneric:
                        //    m_ValidSelections = m_Pawn.Map.GetValidAttack(m_Pawn.CurrentTile.Node, m_ActionItems[m_ActionIndex].Value - 1, m_ActionItems[m_ActionIndex].Value);
                        //    GameMap.map.SetNodesActive(m_ValidSelections, NodeActive.Attack);
                        //    break;
                        //case ActionItemType.AttackLine:
                        //    //m_ValidSelections = m_Pawn.Map.GetValidAttackLine(m_Pawn.CurrentTile.Node, m_ActionItems[m_ActionIndex - 1].Min, m_ActionItems[m_ActionIndex - 1].Max);
                        //    GameMap.map.SetNodesActive(m_ValidSelections, NodeActive.Attack);
                        //    m_IsAoe = m_ActionItems[m_ActionIndex].IsAoe;
                        //    break;
                }
            }
        }


        // Override SelectNode to queue up the next action rather than exiting out
        protected override void SelectNode(TilePathNode node)
        {
            bool isValid = UNet.AttackValidator.Validate(m_Pawn, node, m_Map, m_AllValidAttacks, m_IsAoe);
            //if (isValid) {
            //    // check for targets within aoe
            //    foreach (var tile in validAttacks) {
            //        if (tile.IsOccupied && tile.Occupant.OwnerId != m_Pawn.OwnerId) {

            //        }
            //    }
            //} else if (node.IsOccupied) {

            //} else {
            //    DisableAction();
            //}

            if (isValid) {
                if (m_IsAoe) {
                    foreach (var validAttack in m_AllValidAttacks) {
                        foreach (var tile in validAttack) {
                            if (tile.IsOccupied && tile.Occupant.OwnerId != m_Pawn.OwnerId) {
                                m_AttackSelections.Add(node);
                                ClearDraw(m_AllValidAttacks);
                                m_IsSelectingAttack = false;
                                SetupAction(0);
                                break;
                            }
                        }
                    }
                } else {
                    m_AttackSelections.Add(node);
                    m_IsSelectingAttack = false;
                    ClearDraw(m_AllValidAttacks);
                    SetupAction(0);
                }
            } else {
                DisableAction();
            }
        }
        protected void GetValidAttacks(AttackType attackType)
        {
            switch (attackType) {
                case AttackType.Line:
                    GetValidLineAttacks(m_Pawn.CurrentNode, m_Map, m_AllValidAttacks,
                                            m_MaxAttackDistance, m_MinAttackDistance);
                    break;
                default:
                    GetValidAttacks(m_Pawn.CurrentNode, m_Map, m_AllValidAttacks,
                                        m_MaxAttackDistance, m_MinAttackDistance);
                    break;
            }
        }
        //protected virtual GetAttacks GetAttacksMethod(AttackType attackType)
        //{
        //    switch (m_AttackType) {
        //        case AttackType.Line:
        //            return GetValidLineAttacks;
        //        default:
        //            return GetValidAttacks;
        //    }
        //}
    }
}
