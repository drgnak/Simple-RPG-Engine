// Copyright (c) 2015 Tony Nguyen
// See License.txt for license info

using UnityEngine;
using System.Collections;
using System;
using SettlersEngine;

namespace SimpleRPGEngine
{
    public enum NodeActive { None, Attack, Move };
    public enum WallDirection { Right = 0, Up, Left, Down };

    [Serializable]
    public class TilePathNode : SettlersEngine.IPathNode<System.Object>
    {
        //public float Distance { get; set; }
        public Int32 X { get; internal set; }
        public Int32 Y { get; internal set; }
        public Int32 Z { get; internal set; }
        public Int32 Index { get; set; }
        public Boolean IsOccupied { get; internal set; }
        public NodeActive Active { get; set; }
        public Boolean IsHighlighted { get; set; }
        public GameObject gameObject;
        public Pawn Occupant { get; internal set; }
        public Boolean[] IsWalls { get; internal set; }

        // Constructor
        public TilePathNode(GameObject gameObject)
        {
            this.gameObject = gameObject;
            IsWalls = new Boolean[4];
        }

        // Constructor
        public TilePathNode(GameObject gameObject, int height, bool[] isWalls)
        {
            this.gameObject = gameObject;
            Z = height;
            IsWalls = isWalls;
        }

        /// <summary>
        /// Set an occupant for this node
        /// </summary>
        /// <param name="pawn">Reference to Pawn that occupies this node</param>
        public void AddOccupant(Pawn pawn)
        {
            IsOccupied = true;
            Occupant = pawn;
        }

        /// <summary>
        /// Removes an occupent from node
        /// </summary>
        public void ClearOccupant()
        {
            IsOccupied = false;
            Occupant = null;
        }

        /// <summary>
        /// Check to see if able to travel in a direction
        /// </summary>
        /// <param name="direction">Direction to check</param>
        /// <param name="unused">Unused</param>
        /// <returns>True if able to travel in direction</returns>
        public Boolean CanTravelDirection(Direction direction, System.Object unused)
        {
            switch (direction) {
                case (Direction.East):
                    return !IsWalls[(int)WallDirection.Right];
                case (Direction.North):
                    return !IsWalls[(int)WallDirection.Up];
                case (Direction.West):
                    return !IsWalls[(int)WallDirection.Left];
                case (Direction.South):
                    return !IsWalls[(int)WallDirection.Down];
                default:
                    return true;
            }
        }

        /// <summary>
        /// Gets whether or not the current node is a wall
        /// </summary>
        /// <returns>Returns true if this node is a wall</returns>
        public bool IsTileWall()
        {
            return IsWalls[0] && IsWalls[1] && IsWalls[2] && IsWalls[3];
        }
        public bool IsTileWall(System.Object unused)
        {
            Debug.LogWarning("IsTileWall deprecated. Use CanTravelDirection to find walls instead");
            return IsTileWall();
        }

        /// <summary>
        /// Gets whether or not the current node is occupied
        /// </summary>
        /// <returns>Returns true if this node is occupied</returns>
        public bool IsTileOccupied(System.Object unused)
        {
            return IsOccupied;
        }
    }
}
