// Copyright (c) 2015 Tony Nguyen
// See License.txt for license info

using UnityEngine;
using System.Collections;

namespace SimpleRPGEngine
{
    /// <summary>
    /// Wrapper for TilePathNode. Handles the drawing of the tile
    /// </summary>
    public class Tile : MonoBehaviour
    {
        private enum Border { D, L, U, R, DL, DU, DR, LU, LR, UR, URD, RDL, DLU, LUR, None, All}
        [SerializeField]
        private int m_TileSize;
        [SerializeField]
        private GameMap m_Map;
        [SerializeField]
        private Color m_TileColor;
        [SerializeField]
        private Color m_HoverColor;
        [SerializeField]
        private Color m_AttackColor;
        [SerializeField]
        private Color m_MoveColor;
        [SerializeField]
        private Sprite[] m_Sprites = new Sprite[16];

        public TilePathNode Node { get; set; }
        private SpriteRenderer m_Renderer;
        private bool m_IsHighlighted;

        // Use this for initialization
        void Awake()
        {
            m_Renderer = GetComponent<SpriteRenderer>();
            m_Renderer.color = m_TileColor;
        }

        // Update the color of the tile based on node state
        void FixedUpdate()
        {
            if (Node.IsTileWall()) {
                m_Renderer.color = Color.black;
            } else if (m_IsHighlighted) {
                m_Renderer.color = m_HoverColor;
            } else if (Node.IsHighlighted) {
                m_Renderer.color = m_HoverColor;
            } else if (Node.Active == NodeActive.Move) {
                m_Renderer.color = m_MoveColor;
            } else if (Node.Active == NodeActive.Attack) {
                m_Renderer.color = m_AttackColor;
            } else if (Node.IsOccupied) {
                m_Renderer.color = Color.clear;
            } else {
                m_Renderer.color = m_TileColor;
            }
        }

        /// <summary>
        /// Set the position of tile in both grid and real-world coordinates
        /// </summary>
        /// <param name="x">Grid X</param>
        /// <param name="y">Grid Y</param>
        public void SetPosition(int x, int y)
        {
            Node.X = x;
            Node.Y = y;
            transform.localPosition = new Vector2(x * m_TileSize, y * m_TileSize);
        }

        // Set alpha of tile, keeping the same color
        private void SetAlpha(float alpha)
        {
            m_Renderer.color = new Color(m_Renderer.color.r, m_Renderer.color.g, m_Renderer.color.b, alpha);
        }

        // Set RGB of the tile, keeping the same alpha
        public void SetColor(float r, float g, float b)
        {
            m_Renderer.color = new Color(r, g, b, m_Renderer.color.a);
        }

        // Set RGBA of tile
        public void SetColor(float r, float g, float b, float a)
        {
            m_Renderer.color = new Color(r, g, b, a);
        }
        
        // Temporary highlight. Will change to a selection box in the future
        private void OnMouseOver()
        {
            if (!Node.IsOccupied && !Node.IsTileWall()) {
                m_IsHighlighted = true;
            }

            // Toggle wall
            // Right
            if (Input.GetKeyDown(KeyCode.RightArrow)) {
                Node.IsWalls[(int)WallDirection.Right] = !Node.IsWalls[(int)WallDirection.Right];
                UpdateSpriteBorder();
                // Set adjacent node
                var node = m_Map.GetNode(Node.X + 1, Node.Y);
                if (node != null) {
                    node.IsWalls[(int)WallDirection.Left] = Node.IsWalls[(int)WallDirection.Right];
                    node.gameObject.GetComponent<Tile>().UpdateSpriteBorder();
                }
            }
            // Up
            if (Input.GetKeyDown(KeyCode.UpArrow)) {
                Node.IsWalls[(int)WallDirection.Up] = !Node.IsWalls[(int)WallDirection.Up];
                UpdateSpriteBorder();
                // Set adjacent node
                var node = m_Map.GetNode(Node.X, Node.Y + 1);
                if (node != null) {
                    node.IsWalls[(int)WallDirection.Down] = Node.IsWalls[(int)WallDirection.Up];
                    node.gameObject.GetComponent<Tile>().UpdateSpriteBorder();
                }
            }
            // Left
            if (Input.GetKeyDown(KeyCode.LeftArrow)) {
                Node.IsWalls[(int)WallDirection.Left] = !Node.IsWalls[(int)WallDirection.Left];
                UpdateSpriteBorder();
                // Set adjacent node
                var node = m_Map.GetNode(Node.X - 1, Node.Y);
                if (node != null) {
                    node.IsWalls[(int)WallDirection.Right] = Node.IsWalls[(int)WallDirection.Left];
                    node.gameObject.GetComponent<Tile>().UpdateSpriteBorder();
                }
            }
            // Down
            if (Input.GetKeyDown(KeyCode.DownArrow)) {
                Node.IsWalls[(int)WallDirection.Down] = !Node.IsWalls[(int)WallDirection.Down];
                UpdateSpriteBorder();
                // Set adjacent node
                var node = m_Map.GetNode(Node.X, Node.Y - 1);
                if (node != null) {
                    node.IsWalls[(int)WallDirection.Up] = Node.IsWalls[(int)WallDirection.Down];
                    node.gameObject.GetComponent<Tile>().UpdateSpriteBorder();
                }
            }
        }

        // Temporary highlight. Will change to a selection box in the future
        private void OnMouseExit()
        {
            if (!Node.IsTileWall()) {
                m_IsHighlighted = false;
            }
        }

        /// <summary>
        /// Update m_Renderer.sprite to visually match Node.IsWalls. Requires m_Sprite to have 16
        /// sprites.
        /// </summary>
        public void UpdateSpriteBorder()
        {
            if (m_Sprites.Length == 16) {
                if (Node.IsWalls[(int)WallDirection.Down] && Node.IsWalls[(int)WallDirection.Left] &&
                            Node.IsWalls[(int)WallDirection.Up] && Node.IsWalls[(int)WallDirection.Right]) {
                    m_Renderer.sprite = m_Sprites[(int)Border.All];
                } else if (Node.IsWalls[(int)WallDirection.Left] && Node.IsWalls[(int)WallDirection.Up] &&
                            Node.IsWalls[(int)WallDirection.Right]) {
                    m_Renderer.sprite = m_Sprites[(int)Border.LUR];
                } else if (Node.IsWalls[(int)WallDirection.Down] && Node.IsWalls[(int)WallDirection.Left] &&
                            Node.IsWalls[(int)WallDirection.Up]) {
                    m_Renderer.sprite = m_Sprites[(int)Border.DLU];
                } else if (Node.IsWalls[(int)WallDirection.Down] && Node.IsWalls[(int)WallDirection.Left] &&
                            Node.IsWalls[(int)WallDirection.Right]) {
                    m_Renderer.sprite = m_Sprites[(int)Border.RDL];
                } else if (Node.IsWalls[(int)WallDirection.Down] && Node.IsWalls[(int)WallDirection.Up] &&
                            Node.IsWalls[(int)WallDirection.Right]) {
                    m_Renderer.sprite = m_Sprites[(int)Border.URD];
                } else if (Node.IsWalls[(int)WallDirection.Up] && Node.IsWalls[(int)WallDirection.Right]) {
                    m_Renderer.sprite = m_Sprites[(int)Border.UR];
                } else if (Node.IsWalls[(int)WallDirection.Left] && Node.IsWalls[(int)WallDirection.Right]) {
                    m_Renderer.sprite = m_Sprites[(int)Border.LR];
                } else if (Node.IsWalls[(int)WallDirection.Left] && Node.IsWalls[(int)WallDirection.Up]) {
                    m_Renderer.sprite = m_Sprites[(int)Border.LU];
                } else if (Node.IsWalls[(int)WallDirection.Down] && Node.IsWalls[(int)WallDirection.Right]) {
                    m_Renderer.sprite = m_Sprites[(int)Border.DR];
                } else if (Node.IsWalls[(int)WallDirection.Down] && Node.IsWalls[(int)WallDirection.Up]) {
                    m_Renderer.sprite = m_Sprites[(int)Border.DU];
                } else if (Node.IsWalls[(int)WallDirection.Down] && Node.IsWalls[(int)WallDirection.Left]) {
                    m_Renderer.sprite = m_Sprites[(int)Border.DL];
                } else if (Node.IsWalls[(int)WallDirection.Down]) {
                    m_Renderer.sprite = m_Sprites[(int)Border.D];
                } else if (Node.IsWalls[(int)WallDirection.Left]) {
                    m_Renderer.sprite = m_Sprites[(int)Border.L];
                } else if (Node.IsWalls[(int)WallDirection.Up]) {
                    m_Renderer.sprite = m_Sprites[(int)Border.U];
                } else if (Node.IsWalls[(int)WallDirection.Right]) {
                    m_Renderer.sprite = m_Sprites[(int)Border.R];
                } else {
                    m_Renderer.sprite = m_Sprites[(int)Border.None];
                }
            } else {
                Debug.LogError("Tile does not have the correct amount of sprites.");
            }
        }
    }
}
