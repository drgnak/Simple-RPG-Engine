/*
The MIT License

Copyright (c) 2010 Christoph Husse, 2015 Tony Nguyen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace SettlersEngine
{

    /// <summary>
    /// Uses about 50 MB for a 1024x1024 grid.
    /// </summary>
    public class Dijkstra<TPathNode, TUserContext> where TPathNode : IPathNode<TUserContext>
    {
        private PriorityQueue<PathNode> m_OrderedOpenSet;
        private PathNode[,] m_CameFrom;
        private Dictionary<PathNode, float> m_Distance;
        private PathNode[,] m_SearchSpace;
        private Boolean m_hasSearched;

        public TPathNode[,] SearchSpace { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }

        protected class PathNode : IPathNode<TUserContext>, IComparer<PathNode>, IIndexedObject
        {
            public static readonly PathNode Comparer = new PathNode(0, 0, default(TPathNode));

            public TPathNode UserContext { get; internal set; }
            public Double G { get; internal set; }
            public Double H { get; internal set; }
            public Double F { get; internal set; }
            public int Index { get; set; }
            public float Distance { get; set; }

            public Boolean CanTravelDirection(Direction direction, TUserContext inContext)
            {
                return UserContext.CanTravelDirection(direction, inContext);
            }

            public Boolean IsTileWall(TUserContext inContext)
            {
                return UserContext.IsTileWall(inContext);
            }

            public Boolean IsTileOccupied(TUserContext inContext)
            {
                return UserContext.IsTileOccupied(inContext);
            }

            public int X { get; internal set; }
            public int Y { get; internal set; }

            public int Compare(PathNode x, PathNode y)
            {
                if (x.F < y.F)
                    return -1;
                else if (x.F > y.F)
                    return 1;

                return 0;
            }

            public PathNode(int inX, int inY, TPathNode inUserContext)
            {
                X = inX;
                Y = inY;
                UserContext = inUserContext;
            }
        }

        public Dijkstra(TPathNode[,] inGrid)
        {
            SearchSpace = inGrid;
            Width = inGrid.GetLength(0);
            Height = inGrid.GetLength(1);
            m_SearchSpace = new PathNode[Width, Height];
            m_CameFrom = new PathNode[Width, Height];
            m_Distance = new Dictionary<PathNode, float>();
            m_OrderedOpenSet = new PriorityQueue<PathNode>(PathNode.Comparer);

            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    if (inGrid[x, y] == null)
                        throw new ArgumentNullException();

                    m_SearchSpace[x, y] = new PathNode(x, y, inGrid[x, y]);
                }
            }
        }

        protected virtual Double Heuristic(PathNode inStart, PathNode inEnd)
        {
            return Math.Sqrt((inStart.X - inEnd.X) * (inStart.X - inEnd.X) + (inStart.Y - inEnd.Y) * (inStart.Y - inEnd.Y));
        }

        private static readonly Double SQRT_2 = Math.Sqrt(2);

        protected virtual Double NeighborDistance(PathNode inStart, PathNode inEnd)
        {
            int diffX = Math.Abs(inStart.X - inEnd.X);
            int diffY = Math.Abs(inStart.Y - inEnd.Y);

            switch (diffX + diffY)
            {
                case 1: return 1;
                case 2: return SQRT_2;
                case 0: return 0;
                default:
                    throw new ApplicationException();
            }
        }

        //private List<Int64> elapsed = new List<long>();

        /// <summary>
        /// Returns null, if no path is found. Start- and End-Node are included in returned path. The user context
        /// is passed to IsWalkable().
        /// </summary>
        /// 
        public LinkedList<TPathNode> GetPath(Vector2Int inEndNode, TUserContext inUserContext)
        {
            if (m_hasSearched) {
                PathNode end = m_SearchSpace[inEndNode.X, inEndNode.Y];
                return ReconstructPath(m_CameFrom, end);
            } else {
                return null;
            }
        }

        public List<TPathNode> Search(Vector2Int inStartNode, TUserContext inUserContext, float maxDistance, float minDistance = 0, bool allowDiagonal = false)
        {
            m_OrderedOpenSet.Clear();
            m_Distance.Clear();

            PathNode startNode = m_SearchSpace[inStartNode.X, inStartNode.Y];
            m_OrderedOpenSet.Push(startNode);
            m_Distance[startNode] = 0;

            for (int x = 0; x < Width; x++) {
                for (int y = 0; y < Height; y++) {
                    m_CameFrom[x, y] = null;
                }
            }

            while (m_OrderedOpenSet.Count != 0) {
                PathNode x = m_OrderedOpenSet.Pop();
                PathNode[] neighborNodes = new PathNode[8];
                // TODO: way to set movement costs
                float[] cost = new float[8] {1.5f, 1f, 1.5f, 1f, 1f, 1.5f, 1f, 1.5f};

                if (m_Distance[x] + 1 <= maxDistance) {
                    if (allowDiagonal) {
                        StoreNeighborNodesDiagonal(x, neighborNodes, cost, inUserContext);
                    } else {
                        StoreNeighborNodes(x, neighborNodes, cost, inUserContext);
                    }

                    for (int i = 0; i < neighborNodes.Length; i++) {
                        PathNode y = neighborNodes[i];
                        float dist = cost[i];
                        if (y == null)
                            continue;

                        //if (y.UserContext.IsTileWall(inUserContext))
                        //    continue;
                        if (y.UserContext.IsTileOccupied(inUserContext))
                            continue;

                        if (m_Distance.ContainsKey(y)) {
                            if (m_Distance[x] + dist >= m_Distance[y])
                                continue;

                            m_Distance[y] = m_Distance[x] + dist;
                            m_CameFrom[y.X, y.Y] = x;
                        } else {
                            m_Distance.Add(y, m_Distance[x] + dist);
                            m_CameFrom[y.X, y.Y] = x;
                        }

                        m_OrderedOpenSet.Push(y);
                        
                    }
                }
            }

            var list = new List<TPathNode>();
            foreach (PathNode key in m_Distance.Keys) {
                if (m_Distance[key] >= minDistance)
                    list.Add(key.UserContext);
            }
            m_hasSearched = true;
            return list;
        }

        private LinkedList<TPathNode> ReconstructPath(PathNode[,] came_from, PathNode current_node)
        {
            LinkedList<TPathNode> result = new LinkedList<TPathNode>();

            ReconstructPathRecursive(came_from, current_node, result);

            return result;
        }

        private void ReconstructPathRecursive(PathNode[,] came_from, PathNode current_node, LinkedList<TPathNode> result)
        {
            PathNode item = came_from[current_node.X, current_node.Y];

            if (item != null)
            {
                ReconstructPathRecursive(came_from, item, result);

                result.AddLast(current_node.UserContext);
            }
            //else
                //result.AddLast(current_node.UserContext);
        }

        private void StoreNeighborNodes(PathNode inAround, PathNode[] inNeighbors, float[] cost, TUserContext inUserContext)
        {
            int x = inAround.X;
            int y = inAround.Y;

            // moving down
            if (y > 0 && inAround.CanTravelDirection(Direction.South, inUserContext))
                inNeighbors[1] = m_SearchSpace[x, y - 1];
            // moving left
            if (x > 0 && inAround.CanTravelDirection(Direction.West, inUserContext))
                inNeighbors[3] = m_SearchSpace[x - 1, y];
            // moving right
            if (x < Width - 1 && inAround.CanTravelDirection(Direction.East, inUserContext))
                inNeighbors[4] = m_SearchSpace[x + 1, y];
            // moving up
            if (y < Height - 1 && inAround.CanTravelDirection(Direction.North, inUserContext))
                inNeighbors[6] = m_SearchSpace[x, y + 1];
        }

        private void StoreNeighborNodesDiagonal(PathNode inAround, PathNode[] inNeighbors, float[] cost, TUserContext inUserContext)
        {
            int x = inAround.X;
            int y = inAround.Y;

            StoreNeighborNodes(inAround, inNeighbors, cost, inUserContext);

            // TODO Implement diagonals
            if ((x > 0) && (y > 0) && inAround.CanTravelDirection(Direction.SouthWest, inUserContext))
                    inNeighbors[0] = m_SearchSpace[x - 1, y - 1];

            if ((x < Width - 1) && (y > 0) && inAround.CanTravelDirection(Direction.SouthEast, inUserContext))
                    inNeighbors[2] = m_SearchSpace[x + 1, y - 1];

            if ((x > 0) && (y < Height - 1) && inAround.CanTravelDirection(Direction.NorthWest, inUserContext))
                    inNeighbors[5] = m_SearchSpace[x - 1, y + 1];

            if ((x < Width - 1) && (y < Height - 1) && inAround.CanTravelDirection(Direction.NorthEast, inUserContext))
                    inNeighbors[7] = m_SearchSpace[x + 1, y + 1];
        }

        private class OpenCloseMap
        {
            private PathNode[,] m_Map;
            public int Width { get; private set; }
            public int Height { get; private set; }
            public int Count { get; private set; }

            public PathNode this[Int32 x, Int32 y]
            {
                get
                {
                    return m_Map[x, y];
                }
            }

            public PathNode this[PathNode Node]
            {
                get
                {
                    return m_Map[Node.X, Node.Y];
                }

            }

            public bool IsEmpty
            {
                get
                {
                    return Count == 0;
                }
            }

            public OpenCloseMap(int inWidth, int inHeight)
            {
                m_Map = new PathNode[inWidth, inHeight];
                Width = inWidth;
                Height = inHeight;
            }

            public void Add(PathNode inValue)
            {
                PathNode item = m_Map[inValue.X, inValue.Y];

#if DEBUG
                if (item != null)
                    throw new ApplicationException();
#endif

                Count++;
                m_Map[inValue.X, inValue.Y] = inValue;
            }

            public bool Contains(PathNode inValue)
            {
                PathNode item = m_Map[inValue.X, inValue.Y];

                if (item == null)
                    return false;

#if DEBUG
                if (!inValue.Equals(item))
                    throw new ApplicationException();
#endif

                return true;
            }

            public void Remove(PathNode inValue)
            {
                PathNode item = m_Map[inValue.X, inValue.Y];

#if DEBUG
                if (!inValue.Equals(item))
                    throw new ApplicationException();
#endif

                Count--;
                m_Map[inValue.X, inValue.Y] = null;
            }

            public void Clear()
            {
                Count = 0;

                for (int x = 0; x < Width; x++)
                {
                    for (int y = 0; y < Height; y++)
                    {
                        m_Map[x, y] = null;
                    }
                }
            }
        }
    }
}
