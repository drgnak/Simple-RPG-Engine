/*
The MIT License

Copyright (c) 2010 Christoph Husse, 2015 Tony Nguyen

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

// TODO: consolidate some dijkstra/attackfinder stuff

namespace SettlersEngine
{
    /// <summary>
    /// Uses about 50 MB for a 1024x1024 grid.
    /// </summary>
    public class AttackFinder<TPathNode, TUserContext> where TPathNode : IPathNode<TUserContext>
    {
        private PriorityQueue<PathNode> m_OrderedOpenSet;
        private Dictionary<PathNode, float> m_Distance;
        private LinkedList<TPathNode> m_SearchResult;
        private PathNode[,] m_SearchSpace;

        public TPathNode[,] SearchSpace { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }

        protected class PathNode : IPathNode<TUserContext>, IComparer<PathNode>, IIndexedObject
        {
            public static readonly PathNode Comparer = new PathNode(0, 0, default(TPathNode));

            public TPathNode UserContext { get; internal set; }

            public Double F { get; internal set; }
            public int Index { get; set; }
            //public float Distance { get; set; }

            public Boolean CanTravelDirection(Direction direction, TUserContext inContext)
            {
                return UserContext.CanTravelDirection(direction, inContext);
            }

            public Boolean IsTileWall(TUserContext inContext)
            {
                return UserContext.IsTileWall(inContext);
            }

            public Boolean IsTileOccupied(TUserContext inContext)
            {
                return UserContext.IsTileOccupied(inContext);
            }

            public int X { get; internal set; }
            public int Y { get; internal set; }

            public PathNode(int inX, int inY, TPathNode inUserContext)
            {
                X = inX;
                Y = inY;
                UserContext = inUserContext;
            }

            public int Compare(PathNode x, PathNode y)
            {
                if (x.F < y.F)
                    return -1;
                else if (x.F > y.F)
                    return 1;

                return 0;
            }
        }

        public AttackFinder(TPathNode[,] inGrid)
        {
            SearchSpace = inGrid;
            Width = inGrid.GetLength(0);
            Height = inGrid.GetLength(1);
            m_SearchSpace = new PathNode[Width, Height];
            m_Distance = new Dictionary<PathNode, float>();
            m_OrderedOpenSet = new PriorityQueue<PathNode>(PathNode.Comparer);
            m_SearchResult = new LinkedList<TPathNode>();
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    if (inGrid[x, y] == null)
                        throw new ArgumentNullException();

                    m_SearchSpace[x, y] = new PathNode(x, y, inGrid[x, y]);
                }
            }
        }

        //private List<Int64> elapsed = new List<long>();

        /// <summary>
        /// Returns null, if no path is found. Start- and End-Node are included in returned path. The user context
        /// is passed to IsWalkable().
        /// </summary>
        /// 

        //public LinkedList<TPathNode> SearchCone(Vector2Int inStartNode, int maxDistance, int direction, TUserContext inUserContext)
        //{
        //    m_OrderedOpenSet.Clear();
        //    m_Distance.Clear();

        //    PathNode startNode = m_SearchSpace[inStartNode.X, inStartNode.Y];
        //    m_Distance[startNode] = 0;

        //    // UP
        //    for (var i = 0; i < maxDistance; i++) {
                
        //        PathNode node = m_OrderedOpenSet.Pop();
        //        var x = node.X;
        //        var y = node.Y;

        //        if (y + 1 < Height - 1) {
        //            m_OrderedOpenSet.Pop();
        //        }

        //        PathNode[] rowNodes = new PathNode[4];
        //        float[] weights = {100, .2f, .5f, 1f, 2f, 2f, 3f };

        //        if (y + i < Height) {
        //            if (x - weights[i] > 0 && x + weights[i] < Width - 1) {
        //            }
        //        }
        //    }
            
        //    return null;

        //}

        public TPathNode[] SearchLine(Vector2Int inStartNode, float minDistance, float maxDistance, Direction direction, TUserContext inUserContext, Boolean willStopAtPawn = true)
        {
            m_OrderedOpenSet.Clear();
            m_Distance.Clear();
            PathNode startNode = m_SearchSpace[inStartNode.X, inStartNode.Y];
            m_OrderedOpenSet.Push(startNode);
            m_Distance.Add(startNode, 0);
            while (m_OrderedOpenSet.Count != 0) {
                PathNode node = m_OrderedOpenSet.Pop();
                int x = node.X;
                int y = node.Y;
                PathNode nextNode = null;

                if (m_Distance[node] + 1 <= maxDistance) {
                    switch (direction) {
                        case Direction.East:
                            if (x + 1 < Width - 1 && node.CanTravelDirection(Direction.East, inUserContext))
                                nextNode = m_SearchSpace[x + 1, y];
                            break;
                        case Direction.NorthEast:
                            //TODO: Implement diagonals
                            break;
                        case Direction.North:
                            if (y + 1 < Height - 1 && node.CanTravelDirection(Direction.North, inUserContext))
                                nextNode = m_SearchSpace[x, y + 1];
                            break;
                        case Direction.NorthWest:
                            break;
                        case Direction.West:
                            if (x - 1 > 0 && node.CanTravelDirection(Direction.West, inUserContext))
                                nextNode = m_SearchSpace[x - 1, y];
                            break;
                        case Direction.SouthWest:
                            break;
                        case Direction.South:
                            if (y - 1 > 0 && node.CanTravelDirection(Direction.South, inUserContext))
                                nextNode = m_SearchSpace[x, y - 1];
                            break;
                        case Direction.SouthEast:
                            break;
                    }
                }

                if (nextNode == null)
                    continue;
                //if (nextNode.UserContext.IsTileWall(inUserContext))
                //    continue;
                if (willStopAtPawn && node.IsTileOccupied(inUserContext) && node != startNode)
                    continue;

                m_Distance.Add(nextNode, m_Distance[node] + 1);
                //nextNode.Distance = m_Distance[node] + 1;
                m_OrderedOpenSet.Push(nextNode);
                    
            }
            //m_Distance.Remove(startNode);

            m_SearchResult.Clear();
            foreach (PathNode key in m_Distance.Keys) {
                if (m_Distance[key] >= minDistance)
                    m_SearchResult.AddLast(key.UserContext);
            }
            return m_SearchResult.ToArray<TPathNode>(); ;
        }

        public TPathNode[] Search(Vector2Int inStartNode, TUserContext inUserContext, float maxDistance, float minDistance = 1, Boolean willStopAtPawn = true, Boolean allowDiagonal = false)
        {
            m_OrderedOpenSet.Clear();
            m_Distance.Clear();

            PathNode startNode = m_SearchSpace[inStartNode.X, inStartNode.Y];
            m_OrderedOpenSet.Push(startNode);
            m_Distance[startNode] = 0;

            while (m_OrderedOpenSet.Count != 0) {
                PathNode node = m_OrderedOpenSet.Pop();
                PathNode[] neighborNodes = new PathNode[8];
                float[] cost = new float[8] {1.5f, 1f, 1.5f, 1f, 1f, 1.5f, 1f, 1.5f};

                if (m_Distance[node] + 1 <= maxDistance) {
                    if (allowDiagonal) {
                        StoreNeighborNodesDiagonal(node, neighborNodes, cost, inUserContext);
                    } else {
                        StoreNeighborNodes(node, neighborNodes, cost, inUserContext);
                    }

                    for (int i = 0; i < neighborNodes.Length; i++) {
                        PathNode nextNode = neighborNodes[i];
                        float dist = cost[i];
                        if (nextNode == null)
                            continue;

                        //if (nextNode.UserContext.IsTileWall(inUserContext))
                        //    continue;
                        if (willStopAtPawn && node.IsTileOccupied(inUserContext) && node != startNode)
                            continue;
                        if (m_Distance.ContainsKey(nextNode)) {
                            if (m_Distance[node] + dist >= m_Distance[nextNode])
                                continue;

                            m_Distance[nextNode] = m_Distance[node] + dist;
                            //nextNode.Distance = m_Distance[node] + 1;
                        } else {
                            m_Distance.Add(nextNode, m_Distance[node] + dist);
                            //nextNode.Distance = m_Distance[node] + 1;
                        }

                        m_OrderedOpenSet.Push(nextNode);
                    }
                }
            }

            var list = new LinkedList<TPathNode>();
            foreach (PathNode key in m_Distance.Keys) {
                if (m_Distance[key] >= minDistance)
                    list.AddLast(key.UserContext);
            }
            return list.ToArray<TPathNode>();
        }

        private void StoreNeighborNodes(PathNode inAround, PathNode[] inNeighbors, float[] cost, TUserContext inUserContext)
        {
            int x = inAround.X;
            int y = inAround.Y;

            // moving down
            if (y > 0 && inAround.CanTravelDirection(Direction.South, inUserContext))
                inNeighbors[1] = m_SearchSpace[x, y - 1];
            // moving left
            if (x > 0 && inAround.CanTravelDirection(Direction.West, inUserContext))
                inNeighbors[3] = m_SearchSpace[x - 1, y];
            // moving right
            if (x < Width - 1 && inAround.CanTravelDirection(Direction.East, inUserContext))
                inNeighbors[4] = m_SearchSpace[x + 1, y];
            // moving up
            if (y < Height - 1 && inAround.CanTravelDirection(Direction.North, inUserContext))
                inNeighbors[6] = m_SearchSpace[x, y + 1];
        }

        private void StoreNeighborNodesDiagonal(PathNode inAround, PathNode[] inNeighbors, float[] cost, TUserContext inUserContext)
        {
            int x = inAround.X;
            int y = inAround.Y;

            StoreNeighborNodes(inAround, inNeighbors, cost, inUserContext);

            // TODO Implement diagonals
            if ((x > 0) && (y > 0) && inAround.CanTravelDirection(Direction.SouthWest, inUserContext))
                inNeighbors[0] = m_SearchSpace[x - 1, y - 1];

            if ((x < Width - 1) && (y > 0) && inAround.CanTravelDirection(Direction.SouthEast, inUserContext))
                inNeighbors[2] = m_SearchSpace[x + 1, y - 1];

            if ((x > 0) && (y < Height - 1) && inAround.CanTravelDirection(Direction.NorthWest, inUserContext))
                inNeighbors[5] = m_SearchSpace[x - 1, y + 1];

            if ((x < Width - 1) && (y < Height - 1) && inAround.CanTravelDirection(Direction.NorthEast, inUserContext))
                inNeighbors[7] = m_SearchSpace[x + 1, y + 1];
        }

        private class OpenCloseMap
        {
            private PathNode[,] m_Map;
            public int Width { get; private set; }
            public int Height { get; private set; }
            public int Count { get; private set; }

            public PathNode this[Int32 x, Int32 y]
            {
                get
                {
                    return m_Map[x, y];
                }
            }

            public PathNode this[PathNode Node]
            {
                get
                {
                    return m_Map[Node.X, Node.Y];
                }

            }

            public bool IsEmpty
            {
                get
                {
                    return Count == 0;
                }
            }

            public OpenCloseMap(int inWidth, int inHeight)
            {
                m_Map = new PathNode[inWidth, inHeight];
                Width = inWidth;
                Height = inHeight;
            }

            public void Add(PathNode inValue)
            {
                PathNode item = m_Map[inValue.X, inValue.Y];

#if DEBUG
                if (item != null)
                    throw new ApplicationException();
#endif

                Count++;
                m_Map[inValue.X, inValue.Y] = inValue;
            }

            public bool Contains(PathNode inValue)
            {
                PathNode item = m_Map[inValue.X, inValue.Y];

                if (item == null)
                    return false;

#if DEBUG
                if (!inValue.Equals(item))
                    throw new ApplicationException();
#endif

                return true;
            }

            public void Remove(PathNode inValue)
            {
                PathNode item = m_Map[inValue.X, inValue.Y];

#if DEBUG
                if (!inValue.Equals(item))
                    throw new ApplicationException();
#endif

                Count--;
                m_Map[inValue.X, inValue.Y] = null;
            }

            public void Clear()
            {
                Count = 0;

                for (int x = 0; x < Width; x++)
                {
                    for (int y = 0; y < Height; y++)
                    {
                        m_Map[x, y] = null;
                    }
                }
            }
        }
    }
}
