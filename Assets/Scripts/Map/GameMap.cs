// Copyright (c) 2015 Tony Nguyen
// See License.txt for license info

using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections.Generic;
using System.Collections;

namespace SimpleRPGEngine
{
    [Serializable]
    public struct NodeInfo
    {
        public int Height { get { return m_Height; } }
        public bool[] IsWalls { get { return m_IsWalls; } } 

        public NodeInfo(int height, bool[] isWalls)
        {
            m_Height = height;
            m_IsWalls = isWalls;
        }

        private int m_Height;
        private bool[] m_IsWalls;

        public static explicit operator NodeInfo(TilePathNode node)
        {
            return new NodeInfo(node.Z, node.IsWalls);
        }
    }
    [Serializable]
    public class NodeGrid
    {
        public int GridWidth { get { return m_GridWidth; } }
        public int GridHeight { get { return m_GridHeight; } }
        public NodeInfo[,] NodeInfoGrid { get; set; }

        public NodeGrid(int width, int height)
        {
            m_GridWidth = width;
            m_GridHeight = height;
            NodeInfoGrid = new NodeInfo[width, height];
        }

        private int m_GridWidth;
        private int m_GridHeight;
        
    }
    public class GameMap : MonoBehaviour
    {
        [SerializeField]
        protected int m_GridWidth;
        [SerializeField]
        protected int m_GridHeight;
        [SerializeField]
        protected GameObject m_Tile;
        [SerializeField]
        public int tileSize;
        [SerializeField]
        private TextAsset m_GridFile;

        protected TilePathNode[,] m_Grid;
        [NonSerialized]
        private SettlersEngine.Dijkstra<TilePathNode, System.Object> m_Dijkstra;
        [NonSerialized]
        private SettlersEngine.AttackFinder<TilePathNode, System.Object> m_AttackFinder;

        protected virtual void Start()
        {
            // Create grid
            var grid = Serializer.LoadTextAsset<NodeGrid>(m_GridFile);
            if (m_GridFile != null && grid != null) {
                LoadNodeGrid(grid);
            } else {
                Debug.LogError(gameObject.name + ": Missing Grid File");
            }
            m_Dijkstra = new SettlersEngine.Dijkstra<TilePathNode, System.Object>(m_Grid);
            m_AttackFinder = new SettlersEngine.AttackFinder<TilePathNode, System.Object>(m_Grid);
        }

        /// <summary>
        ///  Search for valid attack nodes in a line. Stops at walls and occupied (included) spaces
        /// </summary>
        /// <param name="selectedNode">Origin node</param>
        /// <param name="maxDistance">Maximum distance for the search to travel</param>
        /// <param name="direction"> Direction the search travels</param>
        /// <returns>Array of valid attack nodes</returns>

        public TilePathNode[] GetValidAttackLine(TilePathNode selectedNode, 
                                                    SettlersEngine.Direction direction, 
                                                    int maxDistance, int minDistance)
        {
            if (minDistance > maxDistance) {
                Debug.LogWarning(gameObject.name + ": Maximum distance " + maxDistance + 
                                    " is less than minimum distance " + minDistance);
            }
            return m_AttackFinder.SearchLine(new SettlersEngine.Vector2Int(selectedNode.X, selectedNode.Y),
                                                minDistance, maxDistance, direction, null);
        }

        /// <summary>
        ///  Search for valid attack nodes within a distance. Similar to GetValidMoves,
        ///  except it includes occupied spaces
        /// </summary>
        /// <param name="selectedNode">Origin node</param>
        /// <param name="maxDistance">Maximum distance for the search to travel</param>
        /// <returns>Array of valid attack nodes</returns>
        public TilePathNode[] GetValidAttack(TilePathNode selectedNode, int maxDistance, int minDistance = 1)
        {
            if (minDistance > maxDistance) {
                minDistance = maxDistance;
                Debug.LogWarning(gameObject.name + ": Maximum distance " + maxDistance + 
                                    " is less than minimum distance " + minDistance + " setting minimum = maximum");
            } else if (minDistance < 1) {
                minDistance = 1;
                Debug.LogWarning(gameObject.name + ": Minimum distance is less than 1; Setting minimum distance to 1.");
            }
            return m_AttackFinder.Search(new SettlersEngine.Vector2Int(selectedNode.X, selectedNode.Y),
                                            null, maxDistance, minDistance, true);
        }

        /// <summary>
        ///  Search for valid move nodes within a distance. Stops at walls and occupied spaces
        /// </summary>
        /// <param name="selectedNode">Origin node</param>
        /// <param name="maxDistance">Maximum distance for the search to travel</param>
        /// <returns>Array of valid move nodes</returns>>
        public List<TilePathNode> GetValidMoves(TilePathNode selectedNode, int maxDistance, int minDistance = 1)
        {
            if (minDistance > maxDistance) {
                Debug.LogWarning(gameObject.name + ": Maximum distance " + maxDistance + 
                                    " is less than minimum distance " + minDistance);
            } else if (minDistance < 1) {
                minDistance = 1;
                Debug.LogWarning(gameObject.name + ": Minimum distance is less than 1; Setting minimum distance to 1.");
            }
            return m_Dijkstra.Search(new SettlersEngine.Vector2Int(selectedNode.X, selectedNode.Y), 
                                        null, maxDistance, minDistance);
        }

        public LinkedList<TilePathNode> GetCone(TilePathNode startnode, float maxDistance, int direction)
        {
                return null;
        }

        /// <summary>
        /// Get the move path to node from a search that has been already done
        /// </summary>
        /// <param name="node">Destination node</param>
        /// <returns>List of nodes in order from the origin node to the destination node</returns>
        public LinkedList<TilePathNode> GetPath(TilePathNode node)
        {
            return m_Dijkstra.GetPath(new SettlersEngine.Vector2Int(node.X, node.Y), null);
        }

        /// <summary>
        /// Get tile node at x,y
        /// </summary>
        public TilePathNode GetNode(int x, int y)
        {
            // Check if node is within bounds and exists
            if ((x >= 0 && x < m_GridWidth) && (y >= 0 && y < m_GridWidth) && m_Grid[x, y] != null) {
                return m_Grid[x, y].gameObject.GetComponent<Tile>().Node;
            } else {
                Debug.Log(gameObject.name + ": Coordinate (" + x + ", " + y + 
                            ") is not valid. Returning node at (0, 0)");
                return m_Grid[0, 0].gameObject.GetComponent<Tile>().Node;
            }
        }

        /// <summary>
        /// Set the active state of an array of nodes
        /// </summary>
        /// <param name="nodes">Array of nodes to set</param>
        /// <param name="active">Active state to set</param>
        public void SetNodesActive(TilePathNode[] nodes, NodeActive active)
        {
            if (nodes != null) {
                foreach (var node in nodes) {
                    node.Active = active;
                }
            }
        }

        /// <summary>
        /// Set highlight state of an array of nodes
        /// </summary>
        /// <param name="nodes">Array of nodes to be set</param>
        /// <param name="isHighlighted">Highlight state</param>
        public void SetNodesHighlight(TilePathNode[] nodes, bool isHighlighted)
        {
            if (nodes != null) {
                foreach (var node in nodes) {
                    node.IsHighlighted = isHighlighted;
                }
            }
        }

        /// <summary>
        /// Converts TilePathNode m_Grid to NodeGrid for serialization.
        /// </summary>
        /// <returns>Converted m_Grid</returns>
        public NodeGrid GetNodeGrid()
        {
            var grid = new NodeGrid(m_GridWidth, m_GridHeight);
            for (var i = 0; i < m_GridWidth; i++) {
                for (var j = 0; j < m_GridHeight; j++) {
                    grid.NodeInfoGrid[i, j] = (NodeInfo)m_Grid[i, j];
                }
            }
            return grid;
        }

        /// <summary>
        /// Load m_Grid from NodeGrid
        /// </summary>
        /// <param name="grid"></param>
        protected void LoadNodeGrid(NodeGrid grid)
        {
            if (grid != null) {
                m_Grid = new TilePathNode[grid.GridWidth, grid.GridHeight];
                m_GridWidth = grid.GridWidth;
                m_GridHeight = grid.GridHeight;
                for (var i = 0; i < m_GridWidth; i++) {
                    for (var j = 0; j < m_GridHeight; j++) {
                        // Create new tile
                        var newTile = (GameObject)Instantiate(m_Tile);
                        // Initialize vars for new tile
                        if (newTile != null) {
                            try {
                                // Setup tile GameObject
                                newTile.tag = "Tiles";
                                newTile.SetActive(true);
                                newTile.transform.parent = transform.Find("Tiles").transform;

                                // Create new node
                                var tile = newTile.GetComponent<Tile>();
                                tile.Node = new TilePathNode(newTile,
                                                            grid.NodeInfoGrid[i, j].Height, 
                                                            grid.NodeInfoGrid[i, j].IsWalls);
                                tile.SetPosition(i, j);
                                m_Grid[i, j] = tile.Node;
                                tile.UpdateSpriteBorder();
                            } catch (System.Exception e) {
                                Debug.Log(e);
                            }
                        }
                    }
                }
                Debug.Log(gameObject.name + ": Grid loaded");
            } else {
                Debug.Log(gameObject.name + ": Grid load failed");
            }
        }

        /// <summary>
        /// Destroy current grid
        /// </summary>
        protected void DestroyGrid()
        {
            var tiles = GameObject.FindGameObjectsWithTag("Tiles");
            for (var i = 0; i < tiles.Length; i++) {
                Destroy(tiles[i]);
            }
            m_Grid = null;
            Debug.Log(gameObject.name + ": Grid destroyed");
        }
    }
}
