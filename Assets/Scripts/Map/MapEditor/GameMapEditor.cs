// Copyright (c) 2015 Tony Nguyen
// See License.txt for license info

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
    using UnityEditor;
#endif

namespace SimpleRPGEngine
{
    public class GameMapEditor : GameMap
    {
        // Use this for initialization
        protected override void Start()
        {
            // Create grid
            NewGrid();
        }

        /// <summary>
        /// Create completely new grid
        /// </summary>
        private void NewGrid()
        {
            m_Grid = new TilePathNode[m_GridWidth, m_GridHeight];
            for (var i = 0; i < m_GridWidth; i++) {
                for (var j = 0; j < m_GridHeight; j++) {
                    // Create new tile
                    var newTile = (GameObject)Instantiate(m_Tile);
                    // Initialize vars for new tile
                    if (newTile != null) {
                        try {
                            // Setup tile GameObject
                            newTile.tag = "Tiles";
                            newTile.SetActive(true);
                            newTile.transform.parent = transform.Find("Tiles").transform;
                            var tile = newTile.GetComponent<Tile>();

                            // Create new node
                            tile.Node = new TilePathNode(newTile);
                            tile.SetPosition(i, j);
                            m_Grid[i, j] = tile.Node;
                        } catch (System.Exception e) {
                            Debug.Log(e);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Destroy old grid and create new one
        /// </summary>
        public void OnNew()
        {
            DestroyGrid();
            NewGrid();
        }

        /// <summary>
        /// Save Grid to file
        /// </summary>
        public void OnSave()
        {
#if UNITY_EDITOR
            var path = EditorUtility.SaveFilePanel(
                            "Save Attack Data File...", "Assets/", "", "bytes");
            if (path.Length != 0) {
                Serializer.Save<NodeGrid>(path, GetNodeGrid());
            }
            Debug.Log("Saved to \"" + path + "\"");
#else
            Debug.Log("GameMapEditor is editor-only");
#endif
        }

        /// <summary>
        /// Load Grid from file
        /// </summary>
        public void OnLoad()
        {
#if UNITY_EDITOR
            var path = EditorUtility.OpenFilePanel(
                            "Open Attack Data File...", "Assets/", "bytes");
            if (path.Length != 0) {
                DestroyGrid();
                var grid = Serializer.Load<NodeGrid>(path);
                if (grid != null) {
                    LoadNodeGrid(grid);
                }
            } else {
            }
#else
            Debug.Log("GameMapEditor is editor-only");
#endif
        }
    }
}
