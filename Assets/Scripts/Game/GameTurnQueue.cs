// Copyright (c) 2015 Tony Nguyen
// See License.txt for license info

using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

namespace SimpleRPGEngine
{
    public class TeamManager
    {
        public List<Team> Teams { get; private set; }

        public TeamManager()
        {
            Teams = new List<Team>();
        }

        public Team GetTeamWithId(int teamId)
        {
            foreach (var team in Teams) {
                if (team.TeamId == teamId) {
                    return team;
                }
            }
            return null;
        }

        public void RegisterPawnToId(int teamId, Pawn pawn)
        {
            var team = GetTeamWithId(teamId);
            if (team == null) {
                team = new Team(teamId);
                Teams.Add(team);
                Debug.Log("TeamManager: Adding new team");
            }

            team.Add(pawn);
        }
    }
    public class Team
    {
        public int TeamId { get; private set; }
        public List<Pawn> Pawns { get; private set; }

        public Team(int teamId)
        {
            TeamId = teamId;
            Pawns = new List<Pawn>();
        }

        public void Add(Pawn pawn)
        {
            Pawns.Add(pawn);
            Debug.Log("Team: Added pawn for team " + TeamId);
        }
    }
    public class GameTurnQueue : NetworkBehaviour
    {
        private int m_Round;
        private int m_PlayerIndex;
        private bool m_UpdateSelection = true;

        public TeamManager RPGTeamManager { get; private set; }

        private void Start()
        {
            RPGTeamManager = new TeamManager();
            if (RPGTeamManager.Teams.Count != 0) {
                //m_PlayerList[m_CurrentPlayer].Select(GetCurrentPawn());
                RPGTeamManager.Teams[m_PlayerIndex].Pawns[m_Round].Select();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void Update()
        {
            if (RPGTeamManager.Teams.Count != 0) {
                if ((GetCurrentPawn() == null || GetCurrentPawn().AP <= 0) && !m_UpdateSelection) {
                    if (m_PlayerIndex < RPGTeamManager.Teams.Count - 1) {
                        // move on to the next player this round
                        Debug.Log(gameObject.name + ": Next player: " + m_PlayerIndex);
                        m_PlayerIndex += 1;
                    } else if (m_Round < RPGTeamManager.Teams[m_PlayerIndex].Pawns.Count - 1) {
                        // Next round
                        Debug.Log(gameObject.name + ": Next round: " + m_Round);
                        m_Round += 1;
                        m_PlayerIndex = 0;
                    } else {
                        // Start turn order over
                        Debug.Log(gameObject.name + ": Restarting turn order");
                        m_Round = 0;
                        m_PlayerIndex = 0;
                    }
                    m_UpdateSelection = true;
                } 
                
                if (GetCurrentPawn() != null && m_UpdateSelection) {
                    RPGTeamManager.Teams[m_PlayerIndex].Pawns[m_Round].StartTurn();
                    Debug.Log(gameObject.name + ": Selecting " + GetCurrentPawn().gameObject.name);
                    m_UpdateSelection = false;
                } 
            }
        }

        /// <summary>
        /// Get the pawn in the current turn
        /// </summary>
        /// <returns>Returns the pawn in the current turn</returns>
        private Pawn GetCurrentPawn()
        {
            return RPGTeamManager.Teams[m_PlayerIndex].Pawns[m_Round];
        }
    }
}
