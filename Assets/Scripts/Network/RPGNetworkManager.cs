// Copyright (c) 2015 Tony Nguyen
// See License.txt for license info

using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

namespace SimpleRPGEngine.UNet
{
    public class RPGNetworkManager : NetworkManager
    {
        [SerializeField] 
        GameTurnQueue m_GameTurnQueue;

        public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
        {
            var player = (GameObject)GameObject.Instantiate(playerPrefab, GetStartPosition().position, Quaternion.identity);
            NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
            m_GameTurnQueue.RPGTeamManager.RegisterPawnToId(conn.connectionId, player.GetComponent<Pawn>());
            player.GetComponent<PawnUNet>().SetOwnerId(conn.connectionId);
            player.name = "PawnUNet " + conn.connectionId + " " + playerControllerId;
            player.GetComponent<PawnMenu>().BuildMenus();
        }
    }
}
