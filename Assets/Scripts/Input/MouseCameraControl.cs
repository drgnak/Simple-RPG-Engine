// Copyright (c) 2015 Tony Nguyen
// See License.txt for license info

using UnityEngine;
using System.Collections;
/// <summary>
/// Basic mouse-based camera control
/// </summary>
public class MouseCameraControl : MonoBehaviour 
{
    [SerializeField]
    [Tooltip("A pan speed of 20 is about 1:1 movement with Windows Sensitivity 6/11.")]
    private float m_CameraPanSpeed = 20f;
    [SerializeField]
    [Tooltip("How quickly the camera zooms in with the mouse wheel.")]
    [Range(0.0f, 1.0f)]
    private float m_CameraZoomSpeed = 1f;

    public float ZoomRatio { get { return GetComponent<Camera>().orthographicSize / m_StartOrthographicSize; } }

    private float m_StartOrthographicSize;

    private Camera m_Camera;

	// Use this for initialization
	void Start () {
        m_Camera = GetComponent<Camera>();
        m_StartOrthographicSize = m_Camera.orthographicSize = Screen.height / 2;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButton(2)) {
            var t = new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"), 0) * - 1;
            transform.position += t * m_CameraPanSpeed * m_Camera.orthographicSize / m_StartOrthographicSize;
        }
        var mousewheel = Input.GetAxis("Mouse ScrollWheel") * m_Camera.orthographicSize * m_CameraZoomSpeed;
        m_Camera.orthographicSize -= mousewheel;
        
	}
}
