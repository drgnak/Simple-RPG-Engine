// Copyright (c) 2015 Tony Nguyen
// See License.txt for license info

using UnityEngine;
using UnityEngine.Networking;

namespace SimpleRPGEngine
{
    public class Player : NetworkBehaviour
    {
        [SerializeField]
        private int m_PlayerId;
        [SerializeField]
        private int m_PawnsPerPlayer;

        public int PlayerId { get {return m_PlayerId;} }

        private void Start()
        {
            if (isClient) {
                for (short i = 0; i < m_PawnsPerPlayer; i++) {
                    ClientScene.AddPlayer(NetworkManager.singleton.client.connection, i);
                }
            }
        }
    }
}
