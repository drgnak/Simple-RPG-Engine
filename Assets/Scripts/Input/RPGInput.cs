// Copyright (c) 2015 Tony Nguyen
// See License.txt for license info

using UnityEngine;
using System.Collections;

namespace SimpleRPGEngine
{
    /// <summary>
    /// This class translates player input into tile highlight/selections.
    /// Currently, it only supports mouse-based input.
    /// TODO: Controller and keyboard input.
    /// </summary>
    public class RPGInput : MonoBehaviour
    {
        private Vector3 m_MousePosition;
        private TilePathNode m_HighlightedNode;
        private TilePathNode m_SelectedNode;
        private Camera m_Camera;

        private void Awake()
        {
            m_Camera = Camera.main;
        }

        void Update()
        {
            m_MousePosition = Input.mousePosition;
            var tilesMask = 1 << LayerMask.NameToLayer("Tiles");
            var tileHit = Physics2D.Raycast(m_Camera.ScreenToWorldPoint(m_MousePosition), Vector2.zero, 0f, tilesMask);

            if (tileHit.collider != null) {
                m_HighlightedNode = tileHit.collider.gameObject.GetComponent<Tile>().Node;
            } else {
                m_HighlightedNode = null;
            }

            if (Input.GetMouseButtonDown(0)) {
                m_SelectedNode = m_HighlightedNode;
            } else {
                m_SelectedNode = null;
            }
        }

        /// <summary>
        /// Get the current highlighted node
        /// </summary>
        /// <returns>Returns the current highlighted node. Returns null if no node is highlighted</returns>
        public TilePathNode GetHighlightedNode()
        {
            return m_HighlightedNode;
        }

        /// <summary>
        /// Get a player's node selection
        /// </summary>
        /// <returns>Returns a player selection. Returns null if no selection has been made.</returns>
        public TilePathNode GetSelectedNode()
        {
            return m_SelectedNode;
        }
    }
}
