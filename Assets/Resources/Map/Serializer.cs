using System;
using System.IO;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

public class Serializer
{
    public static T LoadTextAsset<T>(TextAsset file) where T : class
    {
            try {
                using (Stream stream = new MemoryStream(file.bytes)) {
                    BinaryFormatter formatter = new BinaryFormatter();
                    return formatter.Deserialize(stream) as T;
                }
            } catch (Exception e) {
                Debug.Log(e.Message);
            }
        return default(T);
    }

    public static T Load<T>(string filepath) where T : class
    {
        if (File.Exists(filepath)) {
            try {
                using (Stream stream = File.OpenRead(filepath)) {
                    BinaryFormatter formatter = new BinaryFormatter();
                    return formatter.Deserialize(stream) as T;
                }
            } catch (Exception e) {
                Debug.Log(e.Message);
            }
        }
        return default(T);
    }

    public static void Save<T>(string filename, T data) where T : class
    {
        using (Stream stream = File.OpenWrite(filename)) {
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, data);
        }
    }
}
